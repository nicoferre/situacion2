package situacion2;



public class Cliente {
    private Float saldo;
    private Float saldoTarjeta;

    public Cliente(Float saldo, Float saldoTarjeta){
        this.saldo=saldo;
        this.saldoTarjeta=saldoTarjeta;
    }

    public Float getSaldoTarjeta(){
        return saldoTarjeta;
    }

    public Float getSaldo(){
        return saldo;
    }
}
