package situacion2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Pedido {
    private String estado;
    private Integer numeroDePedido;
    private List<Producto> productos= new ArrayList<>();

    public Pedido(Integer numeroDePedido){
        this.numeroDePedido=numeroDePedido;
        estado="Pendiente";

    }
    
    public void anadirProductos(Producto producto){
        productos.add(producto); 
    }

    public List<Producto> getProductos(){
        return productos;

    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
     
    public Float calcularMontoTotal(){
        Float montoTotal=0f;
        for(int i=0; i <productos.size(); i++){
            montoTotal += (productos.get(i).getPrecio() * productos.get(i).getCantidad());
            if(productos.get(i).getOrigenProducto()==1){
                montoTotal = (float) (montoTotal-(montoTotal*0.02));

            }else if(productos.get(i).getOrigenProducto()==2){
                montoTotal = (float) (montoTotal-(montoTotal*0.0105));
            }else{
                System.out.println("Error");
            }
        }
        return montoTotal;

    }

    

    public void generarPedido(Cliente cliente, Integer numeroDePedido){
        String montoCheque;
        Scanner entrada = new Scanner(System.in);

        System.out.println("--------------------------------------------------------------------------");
        System.out.println(" ");
        System.out.println("Sus productos son: ");
        System.out.println("                                SUBTOTAL");
        System.out.println(" ");
        for(int i=0; i <productos.size(); i++){
            System.out.println(productos.get(i).getNombreProducto()+" x"+productos.get(i).getCantidad()+"                           ");
            System.out.println("                                "+ (productos.get(i).getPrecio() * productos.get(i).getCantidad()));
        }
        System.out.println(" ");

        System.out.println("MONTO TOTAL A PAGAR:            "+calcularMontoTotal());
        System.out.println(" ");
        System.out.println("Dinero en efectivo disponible: "+cliente.getSaldo());
        System.out.println("Saldo en tarjeta: "+cliente.getSaldoTarjeta());
        System.out.println("--------------------------------------------------------------------------");
        System.out.println(" ");
        System.out.println("Desea pagar 1-EN EECTIVO / 2-CON TARJETA / 3-CON CHEQUE");
        String metodoSeleccionado= entrada.nextLine();

        if(Integer.parseInt(metodoSeleccionado)==1){
            if(cliente.getSaldo()>calcularMontoTotal()){
                System.out.println("SU PEDIDO SE HA GENERADO CORRECTAMENTE. EFECTIVO DISPONIBLE AHORA: "+ (cliente.getSaldo()-calcularMontoTotal()));
                estado = "Pagado";
            }else{
                System.out.println("SALDO INSUFICIENTE!");
            }

        }else if(Integer.parseInt(metodoSeleccionado)==2){
            if(cliente.getSaldoTarjeta()>calcularMontoTotal()){
                System.out.println("SU PEDIDO SE HA GENERADO CORRECTAMENTE. SU SALDO AHORA ES: "+ (cliente.getSaldoTarjeta()-calcularMontoTotal()));
                estado = "Pagado";
            }else{
                System.out.println("SALDO INSUFICIENTE!");
            }
        }else if(Integer.parseInt(metodoSeleccionado)==3){
            System.out.print("MONTO DEL CHEQUE: ");
            montoCheque = entrada.nextLine();
            if(Float.parseFloat(montoCheque)>calcularMontoTotal()){
                System.out.println("SU PEDIDO SE HA GENERADO CORRECTAMENTE");
                estado = "Pagado";
            }else{
                System.out.println("EL MONTO DEL CHEQUE ES INSUFICIENTE");
            }
        }else {
            System.out.println("OPCION INCORRECTA");
        }


    }   
    
}
