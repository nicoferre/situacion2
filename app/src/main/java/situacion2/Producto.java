package situacion2;

public class Producto {
    private Integer origenProducto;
    private String nombreProducto;
    private Integer cantidad;
    private Float precio;

    public Producto(Integer origenProducto, String nombreProducto,  Integer cantidad, Float precio){
        this.origenProducto=origenProducto;
        this.nombreProducto=nombreProducto;
        this.cantidad=cantidad;
        this.precio=precio;

    }

   

    public Float getPrecio(){
        return precio;
    }

    public Integer getCantidad(){
        return cantidad;
    }
    
    public String getNombreProducto(){
        return nombreProducto;
    }

    public Integer getOrigenProducto(){
        return origenProducto;
    }

    public void setOrigen(Integer origenProducto) {
        this.origenProducto = origenProducto;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }

    
}
